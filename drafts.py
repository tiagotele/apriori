import copy

a = ['1', '2', '3', '4']
b = [1, 2]


def isArrayInsideAnotherArray(sub, set):
    ar1 = []
    ar2 = []
    for item in sub:
        ar1.append(int(item))
    for item in set:
        ar2.append(int(item))

    if len(ar1) > len(ar2):
        # big = ar1
        # small = ar2
        return False
    else:
        big = ar2
        small = ar1

    for s in small:
        found = False
        for b in big:
            if s == b:
                found = True
                break
        if found == False:
            return False

    return found


print("true = ", isArrayInsideAnotherArray([1], [1]))
print("true = ", isArrayInsideAnotherArray([1], [1, 2]))
print("true = ", isArrayInsideAnotherArray([1], [1, 2, 3]))
print("true = ", isArrayInsideAnotherArray([2], [1, 2, 3]))
print("true = ", isArrayInsideAnotherArray([3], [1, 2, 3]))
print("true = ", isArrayInsideAnotherArray([1, 2], [1, 2, 3]))
print("true = ", isArrayInsideAnotherArray([2, 3], [1, 2, 3]))
print("true = ", isArrayInsideAnotherArray([1, 3], [1, 2, 3]))
print("true = ", isArrayInsideAnotherArray([1, 2, 3], [1, 2, 3]))
print("false = ", isArrayInsideAnotherArray([1, 2, 3], [1, 2, 4]))
print("false = ", isArrayInsideAnotherArray([1, 2, 3], [4, 5, 6]))
print("false = ", isArrayInsideAnotherArray([1, 2], [2, 3, 5]))

my_arr = [1, 2, 3, 4, 5]


def rotate(l, x):
    return l[x:] + l[:x]


for split_size in range(len(my_arr[1:(len(my_arr))])):
    for item_index in range(len(my_arr)):
        my_arr_copy = rotate(my_arr, item_index)
        before = my_arr_copy[:split_size + 1]
        after = my_arr_copy[split_size + 1:]


def return_multiple(value):
    return 2 * value, 3 * value


my_return = return_multiple(2)
print(my_return)


class Foo(object):
    def __init__(self, myInt=10):
        self.myInt = myInt

    def __str__(self):
        return "representation"

    # def __str__(self):
    #     return "Tiago's class " + str(self.myInt)


my_obj = Foo(80)
print(my_obj)
