#!/usr/bin/env python
# encoding:utf8

# GENERIC WAY TO SPLIT A DATABASE TO DO CROSS VALIDATION

import csv

import itertools

import copy


# Loads a csv file
def load_csv(file_name, database):
    with open(file_name, 'r') as f:
        reader = csv.reader(f)
        for r in reader:
            if len(r) == 0:
                continue
            database.append(r)
    f.close()


def get_total_of_transactions(file_name):
    data_set_size = 0
    with open(file_name, 'r') as f:
        reader = csv.reader(f)
        for r in reader:
            data_set_size += 1
    f.close()
    return data_set_size


def combinations(items, numberOfCombinations):
    return itertools.combinations(items, numberOfCombinations)


# Load items
def load_items_from_transaction_database():
    dataset = []
    load_csv("dataset.csv", dataset)
    items = set([])
    for row in dataset:
        for columnIndex in range(len(row)):
            if columnIndex == 0:
                continue
            items.add(row[columnIndex])
    return items


class Candidates(object):
    items = []
    frequency = 0

    def __init__(self, items=[], frequency=0):
        self.items = items
        self.frequency = frequency

    def print(self):
        print("Candidates object. Items = ", self.items, ". Frequency = ", self.frequency)

    def __str__(self):
        str_obj = "Candidates object. Items = "
        for item in self.items:
            str_obj += str(item)
        str_obj += " Frequency = " + str(self.frequency)
        return str_obj


class TableOfCandidates(object):
    def __init__(self, candidates=[], final_candidates=[]):
        self.candidates = candidates
        self.final_candidates = final_candidates


    def __str__(self):
        obj_str = "Table of candidates object. Candidates = "
        for candidate in self.candidates:
            obj_str += str(candidate)
        obj_str += "Final candidates = "
        for candidate in self.final_candidates:
            obj_str += str(candidate)
        return obj_str


def isArrayInsideAnotherArray(sub, set):
    ar1 = []
    ar2 = []
    for item in sub:
        ar1.append(int(item))
    for item in set:
        ar2.append(int(item))

    if len(ar1) > len(ar2):
        # big = ar1
        # small = ar2
        return False
    else:
        big = ar2
        small = ar1

    for s in small:
        found = False
        for b in big:
            if s == b:
                found = True
                break
        if found == False:
            return False

    return found


def sup(items):
    dataset = []
    load_csv("dataset.csv", dataset)
    sp = 0

    for row in dataset:
        data_row = row[1:len(row)]
        if isArrayInsideAnotherArray(items, data_row):
            sp += 1

    return sp


def initialize_candidates():
    items = load_items_from_transaction_database()
    candidates = []
    for item in items:
        candidate = Candidates(item, 0)
        candidates.append(candidate)
    return candidates


def populate_candidates(tableOfCandidates):
    for candidate in tableOfCandidates.candidates:
        frequency = sup(candidate.items)
        candidate.frequency = frequency


def delete_unuseful_candidates(table_of_candidates):
    table_of_candidates_aux = copy.deepcopy(table_of_candidates)
    # for candidate_index in range(len(table_of_candidates.candidates)):
    #     if table_of_candidates.candidates[candidate_index].frequency < sp_min:
    #         table_of_candidates.candidates[candidate_index].print()
    #         table_of_candidates_aux.candidates.remove(table_of_candidates_aux.candidates[candidate_index])
    #         print("should remove this candidate")
    elegible_candidates = []
    for candidate in table_of_candidates_aux.candidates:
        if candidate.frequency >= sp_min:
            elegible_candidates.append(candidate)
    table_of_candidates_aux.candidates = elegible_candidates
    return copy.deepcopy(table_of_candidates_aux)


def get_current_indexes(table):
    current_indexes = set([])
    for candidate in table.candidates:
        for item in candidate.items:
            current_indexes.add(item)
    return current_indexes


def mix_indexes(table):
    # get indexes size for the first candidate in table(all candidates have the same index length)
    candidates_indexes_size = len(table.candidates[0].items)
    candidates_indexes_size += 1
    # get indexes for every candidate in table
    current_indexes = get_current_indexes(table)
    # get combinations for every index size plus one
    indexes_combinations = combinations(current_indexes, candidates_indexes_size)
    # generate candidates for every new index
    new_candidates = []

    for combined_elements in indexes_combinations:
        new_indexes = set([])
        for element in combined_elements:
            new_indexes.add(element)
        candidate = Candidates(new_indexes, [])
        new_candidates.append(candidate)

    # create new table with new candidates
    new_table = TableOfCandidates(new_candidates, [])
    # new_table.print()
    print(new_table)
    return copy.deepcopy(new_table)


# Rotates foward an array "l" "x" times
def rotate(l, x):
    return l[x:] + l[:x]


class Rule(object):
    def __init__(self, indexes=[], nLeft=[], nRigth=[], nBoth=[], lift=0):
        self.indexes = indexes
        self.previous = nLeft
        self.after = nRigth

    def confidence(self):
        return sup(self.indexes) / sup(self.after)

    def lift_op(self, total=1):
        a = sup(self.indexes) / total
        b = sup(self.previous) / total
        c = sup(self.after) / total
        return a / (b * c)

    def __str__(self):
        return "Indexes: " + self.indexes + " previous = " + self.previous + " after" + self.after


''''The main code starts here!!!'''
total_of_transactions = get_total_of_transactions("dataset.csv")
active_items = load_items_from_transaction_database()
sp_min = 2
candidates = initialize_candidates()
tableOfCandidates = TableOfCandidates(candidates, [])
tableOfCandidatesFinal = TableOfCandidates(candidates, [])

avoiding_infinite_loops = 0
while True:
    # populate candidates
    populate_candidates(tableOfCandidates)

    # remove unuseful candidates
    tableOfCandidates = delete_unuseful_candidates(tableOfCandidates)

    # Is there useful candidates
    if len(tableOfCandidates.candidates) > 0:
        # set final candidates
        tableOfCandidatesFinal = copy.deepcopy(tableOfCandidates)

        # Mix indexes
        tableOfCandidates = mix_indexes(tableOfCandidates)
    else:
        break

print(tableOfCandidatesFinal)

# Generate combinations for each candidate's indexes
rules = []


def breaks_array_rules(my_arr):
    for split_size in range(len(my_arr[1:(len(my_arr))])):
        for item_index in range(len(my_arr)):
            my_arr_copy = rotate(my_arr, item_index)
            before = my_arr_copy[:split_size + 1]
            after = my_arr_copy[split_size + 1:]
            rule = Rule(my_arr, before, after, [], 0)
            rules.append(rule)


for candidates in tableOfCandidatesFinal.candidates:
    candidates_items = list(candidates.items)
    breaks_array_rules(candidates_items)

for rule in rules:
    print(rule.confidence())
rules_with_good_confindence = []
for rule in rules:
    # print(rule.confidence())
    if rule.confidence() >= 0.6:
        rules_with_good_confindence.append(rule)

print("----Removing unelegible confindence---")
print(len(rules_with_good_confindence))
for rule in rules_with_good_confindence:
    print(rule.confidence())

print("--------Lift table--------")
for rule in rules_with_good_confindence:
    print(rule.lift_op(total_of_transactions))
